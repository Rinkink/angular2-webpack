# Angular 2 webpack
A base solution in order to start development of an Angular 2 application. 

## Features
### Angular 4 
Version 4.0.1 when this was created
### Karma
### SASS

## Usage
Initialise - pull the NPM stuff
```javascript
npm install
```
Run - starts a dev instance up on http://localhost:8080/
```javascript
npm start
```
Test - runs Karma tests
```javascript
npm test
```
Build
```javascript
npm build
```

## Caveats
Following the tutorial [1]:

Had to fix the jasmine version in package.json due to an error - see [2]. 
```
    "@types/jasmine": "2.5.41",
```

Changed the .ts rule in config/webpack.common.js [2]:
```
    rules: [
      {
        test: /\.ts$/,
 -       loaders: [{
 -         loader: 'awesome-typescript-loader',
 -         options: { configFileName: helpers.root('src', 'tsconfig.json') }
 -       } , 'angular2-template-loader']
 +       loaders: ['awesome-typescript-loader', 'angular2-template-loader']
      },
```

Changed plugin in config/webpack.common.js to fix warnings emmited [4].

## References/Sources
Built based on this

1. https://angular.io/docs/ts/latest/guide/webpack.html
2. https://github.com/angular/angular.io/issues/3198 
3. http://stackoverflow.com/questions/42181156/error-in-at-loader-node-modules-types-jasmine
4. https://github.com/AngularClass/angular2-webpack-starter/issues/993